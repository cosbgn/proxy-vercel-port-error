const useRandomPort = true
export default {
	mode: 'universal',
	server: {
		port: useRandomPort ? Math.floor(Math.random() * 8000) + 1025 : 3000 , // default: 3000
		host: 'localhost' // default: localhost
	},
	build: {
    	extend (config, ctx) {
    	}
  }
}
